
ifeq ($(wildcard bed),bed)
LUA     := $(CURDIR)/bed/bin/lua
LUACHECK:= $(CURDIR)/bed/bin/luacheck
LUAROCKS:= $(CURDIR)/bed/bin/luarocks
else
LUA     := lua
LUACHECK:= luacheck
LUAROCKS:= luarocks
endif
VERSION := $(shell LUA_PATH=";;src/?.lua" $(LUA) -e "m = require [[Coat]]; print(m._VERSION)")
TARBALL := lua-coat-$(VERSION).tar.gz
REV     := 1

LUAVER  := 5.3
PREFIX  := /usr/local
DPREFIX := $(DESTDIR)$(PREFIX)
BINDIR  := $(DPREFIX)/bin
LIBDIR  := $(DPREFIX)/share/lua/$(LUAVER)
INSTALL := install

BED_OPTS:= --lua latest

all:
	@echo "Nothing to build here, you can just make install"

install:
	$(INSTALL) -m 755 -D src/coat2dot                       $(BINDIR)/coat2dot
	$(INSTALL) -m 644 -D src/Coat.lua                       $(LIBDIR)/Coat.lua
	$(INSTALL) -m 644 -D src/Coat/Role.lua                  $(LIBDIR)/Coat/Role.lua
	$(INSTALL) -m 644 -D src/Coat/Types.lua                 $(LIBDIR)/Coat/Types.lua
	$(INSTALL) -m 644 -D src/Coat/UML.lua                   $(LIBDIR)/Coat/UML.lua
	$(INSTALL) -m 644 -D src/Coat/file.lua                  $(LIBDIR)/Coat/file.lua
	$(INSTALL) -m 644 -D src/Coat/Meta/Class.lua            $(LIBDIR)/Coat/Meta/Class.lua
	$(INSTALL) -m 644 -D src/Coat/Meta/Role.lua             $(LIBDIR)/Coat/Meta/Role.lua

uninstall:
	rm -f $(BINDIR)/coat2dot
	rm -f $(LIBDIR)/Coat.lua
	rm -f $(LIBDIR)/Coat/Role.lua
	rm -f $(LIBDIR)/Coat/Types.lua
	rm -f $(LIBDIR)/Coat/UML.lua
	rm -f $(LIBDIR)/Coat/file.lua
	rm -f $(LIBDIR)/Coat/Meta/Class.lua
	rm -f $(LIBDIR)/Coat/Meta/Role.lua

manifest_pl := \
use strict; \
use warnings; \
my @files = qw{MANIFEST}; \
while (<>) { \
    chomp; \
    next if m{^\.}; \
    next if m{^debian/}; \
    next if m{^rockspec/}; \
    push @files, $$_; \
} \
print join qq{\n}, sort @files;

rockspec_pl := \
use strict; \
use warnings; \
use Digest::MD5; \
open my $$FH, q{<}, q{$(TARBALL)} \
    or die qq{Cannot open $(TARBALL) ($$!)}; \
binmode $$FH; \
my %config = ( \
    version => q{$(VERSION)}, \
    rev     => q{$(REV)}, \
    md5     => Digest::MD5->new->addfile($$FH)->hexdigest(), \
); \
close $$FH; \
while (<>) { \
    s{@(\w+)@}{$$config{$$1}}g; \
    print; \
}

version:
	@echo $(VERSION)

CHANGES: dist.info
	perl -i.bak -pe "s{^$(VERSION).*}{q{$(VERSION)  }.localtime()}e" CHANGES

dist.info:
	perl -i.bak -pe "s{^version.*}{version = \"$(VERSION)\"}" dist.info

tag:
	git tag -a -m 'tag release $(VERSION)' $(VERSION)

MANIFEST:
	git ls-files | perl -e '$(manifest_pl)' > MANIFEST

$(TARBALL): MANIFEST
	[ -d lua-Coat-$(VERSION) ] || ln -s . lua-Coat-$(VERSION)
	perl -ne 'print qq{lua-Coat-$(VERSION)/$$_};' MANIFEST | \
	    tar -zc -T - -f $(TARBALL)
	rm lua-Coat-$(VERSION)

dist: $(TARBALL)

rockspec: $(TARBALL)
	perl -e '$(rockspec_pl)' rockspec.in > rockspec/lua-coat-$(VERSION)-$(REV).rockspec

rock:
	$(LUAROCKS) pack rockspec/lua-coat-$(VERSION)-$(REV).rockspec

deb:
	echo "lua-coat ($(shell git describe --dirty)) unstable; urgency=medium" >  debian/changelog
	echo ""                         >> debian/changelog
	echo "  * UNRELEASED"           >> debian/changelog
	echo ""                         >> debian/changelog
	echo " -- $(shell git config --get user.name) <$(shell git config --get user.email)>  $(shell date -R)" >> debian/changelog
	fakeroot debian/rules clean binary

#export GEN_PNG=1

bed:
	hererocks bed $(BED_OPTS) --no-readline --luarocks latest --verbose
	bed/bin/luarocks install lua-testassertion
	bed/bin/luarocks install luacheck
	bed/bin/luarocks install luacov
	hererocks bed --show
	bed/bin/luarocks list

check: test

test:
	LUA_PATH="$(CURDIR)/src/?.lua;$(CURDIR)/test/?.lua;;" \
		prove --exec=$(LUA) test/*.t

luacheck:
	-$(LUACHECK) --std=max --codes src --ignore 211/_ENV 212/t 421/v 423/i 631
	$(LUACHECK) --std=min --config .test.luacheckrc test/*.t

coverage:
	rm -f luacov.*
	-LUA_PATH="$(CURDIR)/src/?.lua;$(CURDIR)/test/?.lua;;" \
		prove --exec="$(LUA) -lluacov" test/*.t
	luacov-console $(CURDIR)/src
	luacov-console -s $(CURDIR)/src

README.html: README.md
	Markdown.pl README.md > README.html

pages:
	mkdocs build -d public

clean:
	rm -f MANIFEST *.bak luacov.* src/*.png test/*.png *.rockspec README.html

realclean: clean
	rm -rf bed

.PHONY: test rockspec deb CHANGES dist.info

