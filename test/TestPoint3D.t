#!/usr/bin/env lua

require 'Test.Assertion'

plan(31)

if not require_ok 'Point3D' then
    skip_rest "no lib"
    os.exit()
end

if os.getenv "GEN_PNG" and os.execute "dot -V" == 0 then
    local f = io.popen("dot -T png -o Point3D.png", 'w')
    f:write(require 'Coat.UML'.to_dot())
    f:close()
end

local a = Point3D{x = 1, y = 2, z = 3}
equals( a:type(), 'Point3D', "new" )
truthy( a:isa 'Point3D' )
truthy( a:isa 'Point' )
is_number( a.x, "accessors")
is_number( a.y )
is_number( a.z )
equals( a.x, 1 )
equals( a.y, 2 )
equals( a.z, 3 )
equals( tostring(a), "(1, 2, 3)", "stringify" )

truthy( a:can 'draw', "method draw" )
truthy( Point:can 'draw' )
is_function( a.draw )
equals( a:draw(), "drawing Point3D(1, 2, 3)" )

a = Point3D()
equals( a:type(), 'Point3D', "new (default)" )
is_number( a.x )
is_number( a.y )
is_number( a.z )
equals( a.x, 0 )
equals( a.y, 0 )
equals( a.z, 0 )
equals( tostring(a), "(0, 0, 0)", "stringify" )

a.x = 1
a.y = 2
a.z = 3
is_number( a.x, "mutator")
is_number( a.y )
is_number( a.z )
equals( a.x, 1 )
equals( a.y, 2 )
equals( a.z, 3 )
equals( tostring(a), "(1, 2, 3)", "stringify" )

error_matches([[a = Point3D{x = "x", y = "y", z = "z"}]],
        "Invalid type for attribute",
        "new (bad)")

