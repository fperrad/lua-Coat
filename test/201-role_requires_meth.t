#!/usr/bin/env lua

require 'Coat.Role'

role 'Breakable'
requires '_break'
excludes 'Repairable'

role 'Repairable'

class 'Car'
with 'Breakable'

function method:_break ()
    return "I broke"
end

class 'SpecialCar'
extends 'Car'

class 'BadCar'
extends 'Car'
with 'Repairable'

class 'FakeCar'
with 'Breakable'


require 'Test.Assertion'

plan(8)

if os.getenv "GEN_PNG" and os.execute "dot -V" == 0 then
    local f = io.popen("dot -T png -o 201.png", 'w')
    f:write(require 'Coat.UML'.to_dot())
    f:close()
end

local car = Car.new()
truthy( car:isa 'Car', "Car" )
truthy( car:does 'Breakable' )
equals( car:_break(), "I broke" )

car = SpecialCar.new()
truthy( car:isa 'SpecialCar', "SpecialCar" )
truthy( car:does 'Breakable' )
equals( car:_break(), "I broke" )

error_matches([[local car = BadCar.new()]],
        "Role Breakable excludes role Repairable",
        "BadCar")

error_matches([[local car = FakeCar.new()]],
        "Role Breakable requires method _break")
