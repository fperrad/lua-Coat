#!/usr/bin/env lua

require 'Coat'

class 'Foo'

has.bar = { is = 'rw' }

require 'Test.Assertion'

plan(8)

if os.getenv "GEN_PNG" and os.execute "dot -V" == 0 then
    local f = io.popen("dot -T png -o 105.png", 'w')
    f:write(require 'Coat.UML'.to_dot())
    f:close()
end

local foo = Foo.new{ bar = 1 }
truthy( foo:isa 'Foo', "Foo" )
equals( foo.bar, 1 )
equals( foo:_set_bar(3), 3 )
equals( foo:_get_bar(), 3 )
equals( foo.bar, 3 )
equals( foo:_set_bar(nil), nil )
equals( foo:_get_bar(), nil )
equals( foo.bar, nil )

