#!/usr/bin/env lua

require 'Coat'

class 'Foo'

has.t = { is = 'rw', default = function () return 2 + 2 end }

require 'Test.Assertion'

plan(5)

if os.getenv "GEN_PNG" and os.execute "dot -V" == 0 then
    local f = io.popen("dot -T png -o 012.png", 'w')
    f:write(require 'Coat.UML'.to_dot())
    f:close()
end

local foo = Foo.new{ t = 2 }
truthy( foo:isa 'Foo', "Foo" )
equals( foo.t, 2 )

foo = Foo.new()
truthy( foo:isa 'Foo', "Foo (default)" )
local val = foo.t
not_equals( type(val), 'function' )
equals( val, 4 )

