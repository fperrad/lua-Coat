#!/usr/bin/env lua

require 'Coat'
require 'Coat.Types'
require 'Coat.file'

class 'Foo'

has.log = { is = 'rw', isa = 'file' }

class 'Bar'

subtype.OpenedFile = {
    as = 'file',
    where = function (f) return io.type(f) == 'file' end,
}

has.log = { is = 'rw', isa = 'OpenedFile' }


require 'Test.Assertion'

plan(6)

if os.getenv "GEN_PNG" and os.execute "dot -V" == 0 then
    local f = io.popen("dot -T png -o 141.png", 'w')
    f:write(require 'Coat.UML'.to_dot())
    f:close()
end

local f = io.tmpfile()
is_userdata( f, "file is a userdata" )
equals( io.type(f), 'file' )
local foo = Foo.new{ log = f }
truthy( foo:isa 'Foo', "isa" )

local bar = Bar.new{ log = f }
truthy( bar:isa 'Bar', "isa" )
f:close()
equals( io.type(f), 'closed file' )
error_matches(function () local _ = Bar.new{ log = f } end,
        "Value for attribute 'log' does not validate type constraint 'OpenedFile'")


