#!/usr/bin/env lua

require 'Test.Assertion'

plan(7)

if not require_ok 'Car' then
    skip_rest "no lib"
    os.exit()
end

if os.getenv "GEN_PNG" and os.execute "dot -V" == 0 then
    local f = io.popen("dot -T png -o Car.png", 'w')
    f:write(require 'Coat.UML'.to_dot())
    f:close()
end

local car = Car.new()
truthy( car:isa 'Car', "isa" )
truthy( car:does 'Breakable', "does" )
equals( car.is_broken, nil )
truthy( car:can '_break', "can" )
equals( car:_break(), "I broke" )
equals( car.is_broken, true )
