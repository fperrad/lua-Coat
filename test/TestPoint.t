#!/usr/bin/env lua

require 'Test.Assertion'

plan(29)

if not require_ok 'Point' then
    skip_rest "no lib"
    os.exit()
end

if os.getenv "GEN_PNG" and os.execute "dot -V" == 0 then
    local f = io.popen("dot -T png -o Point.png", 'w')
    f:write(require 'Coat.UML'.to_dot())
    f:close()
end

local a = Point{x = 1, y = 2}
equals( a:type(), 'Point', "new" )
truthy( a:isa 'Point' )
is_function( a._get_x, "accessors" )
is_function( a._get_y )
is_function( a._set_x )
is_function( a._set_y )
is_number( a.x )
is_number( a.y )
equals( a.x, 1 )
equals( a.y, 2 )
equals( tostring(a), "(1, 2)", "stringify" )

truthy( a:can 'draw', "method draw" )
truthy( Point:can 'draw' )
is_function( a.draw )
equals( a:draw(), "drawing Point(1, 2)" )

a = Point()
equals( a:type(), 'Point', "new (default)" )
is_number( a.x )
is_number( a.y )
equals( a.x, 0 )
equals( a.y, 0 )
equals( tostring(a), "(0, 0)", "stringify" )

a.x = 1
a.y = 2
is_number( a.x, "mutator")
is_number( a.y )
equals( a.x, 1 )
equals( a.y, 2 )
equals( tostring(a), "(1, 2)" )
error_matches(function () a.x = "number expected" end,
        "Invalid type for attribute",
        "check type")

error_matches(function () local _ = Point{x = "x", y = "y"} end,
        "Invalid type for attribute",
        "new (bad)")

