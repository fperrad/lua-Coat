#!/usr/bin/env lua

require 'Coat'

class 'Spanish'

function method.uno ()
    return 1
end

function method.dos ()
    return 2
end

has.nombre = { is = 'rw', isa = 'string' }

class 'English'
has.translate = {
    is = 'ro',
    default = function () return Spanish.new() end,
    handles = {
        one = 'uno',
        two = 'dos',
        _get_name = '_get_nombre',
        _set_name = '_set_nombre',
        bad = '_bad_',
        '_bad', -- equiv: _bad = '_bad'
    },
}

require 'Test.Assertion'

plan(16)

if os.getenv "GEN_PNG" and os.execute "dot -V" == 0 then
    local f = io.popen("dot -T png -o 027.png", 'w')
    f:write(require 'Coat.UML'.to_dot())
    f:close()
end

local foo = Spanish.new()
truthy( foo:isa "Spanish", "Spanish" )
truthy( foo.uno )
equals( foo:uno(), 1 )
truthy( foo.dos )
equals( foo:dos(), 2 )
foo.nombre = 'Juan'
equals( foo.nombre, 'Juan' )

foo = English.new()
truthy( foo:isa 'English', "English" )
truthy( foo.translate:isa 'Spanish')
truthy( foo.one )
equals( foo:one(), 1 )
truthy( foo.two )
equals( foo:two(), 2 )
foo.name = 'John'
equals( foo.name, 'John' )
truthy( foo.bad )

error_matches([[local foo = English.new(); foo:bad()]],
        "Cannot delegate bad from translate %(_bad_%)")

error_matches([[local foo = English.new(); foo:_bad()]],
        "Cannot delegate _bad from translate %(_bad%)")

