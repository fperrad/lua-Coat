-- file FakeCreditCardProcessor.lua
require 'Coat'

require 'CreditCard'
class 'FakeCreditCardProcessor'
with 'CreditCardProcessor'

has.nextResult      = { is = 'rw', isa = 'boolean' }
has.lastCard        = { is = 'rw', isa = 'CreditCard' }
has.lastAmount      = { is = 'rw', isa = 'number' }

function method:charge (creditCard, amount)
    self.lastCard = creditCard
    self.lastAmount = amount
    return ChargeResult.new{
        wasSuccessful = self.nextResult,
    }
end
