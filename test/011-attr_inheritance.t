#!/usr/bin/env lua

require 'Coat'

class 'Foo'
has.field_from_foo_string = { is = 'rw', isa = 'string' }
has.field_from_foo_int = { is = 'rw', isa = 'number', default = 1 }

class 'Bar'
extends 'Foo'
has.field_from_bar = { is = 'rw' }

class 'Baz'
extends 'Bar'
has.field_from_baz = { is = 'rw' }
-- we redefine an attribute of an inherited class
has.field_from_foo_int = { '+', default = 2 }

class 'Biz'
has.field_from_biz = { is = 'rw' }

class 'BalBaz'
extends( 'Bar', 'Biz' )

require 'Test.Assertion'

plan(22)

if os.getenv "GEN_PNG" and os.execute "dot -V" == 0 then
    local f = io.popen("dot -T png -o 011.png", 'w')
    f:write(require 'Coat.UML'.to_dot())
    f:close()
end

local foo = Foo.new()
truthy( foo:isa 'Foo', "Foo" )
equals( foo.field_from_foo_string, nil )
equals( foo.field_from_foo_int, 1 )

local bar = Bar.new()
truthy( bar:isa 'Bar', "Bar" )
equals( bar.field_from_foo_string, nil )
equals( bar.field_from_bar, nil )
equals( bar.field_from_foo_int, 1 )

local baz = Baz.new()
truthy( baz:isa 'Baz', "Baz" )
equals( baz.field_from_foo_string, nil )
equals( baz.field_from_bar, nil )
equals( baz.field_from_baz, nil )
equals( baz.field_from_foo_int, 2 )

local biz = Biz.new()
truthy( biz:isa 'Biz', "Biz" )
equals( biz.field_from_biz, nil )

local bal = BalBaz.new()
truthy( bal:isa 'BalBaz', "BalBaz" )
equals( bal.field_from_foo_string, nil )
equals( bal.field_from_bar, nil )
equals( bal.field_from_biz, nil )
equals( bal.field_from_foo_int, 1 )

error_matches([[Baz.has.no_field = { '+', default = 42 }]],
        "Cannot overload unknown attribute no_field")

error_matches([[Baz.has.field_from_bar = { is = 'rw' }]],
        "Duplicate definition of attribute field_from_bar")

function Baz.method:callIt ()
    return true
end
error_matches([[Baz.has.callIt = { is = 'rw' }]],
        "Overwrite definition of method callIt")

