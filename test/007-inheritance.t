#!/usr/bin/env lua

require 'Coat'

class 'Person'

has.name = { is = 'rw', isa = 'string' }
has.force = { is = 'rw', isa = 'number', default = 1 }

function method:walk ()
    return self.name .. " walks\n"
end

class 'Soldier'

extends 'Person'

has.force = { '+', default = 3 }

function method:attack ()
    return self.force + math.random( 10 )
end

class 'General'

extends 'Soldier'

has.force = { '+', default = 5 }

require 'Test.Assertion'

plan(29)

if os.getenv "GEN_PNG" and os.execute "dot -V" == 0 then
    local f = io.popen("dot -T png -o 007.png", 'w')
    f:write(require 'Coat.UML'.to_dot())
    f:close()
end

local mc = require 'Coat.Meta.Class'

local man = Person.new{ name = 'John' }
truthy( man:isa 'Person', "Person" )
truthy( man:isa(Person) )
truthy( man:isa(man) )
truthy( mc.has( Person, 'name' ) )
truthy( mc.has( Person, 'force' ) )
equals( man.force, 1 )
truthy( man:walk() )

truthy( Soldier:isa 'Soldier', "Class Soldier" )
truthy( Soldier:isa 'Person' )
truthy( Soldier:isa(Person) )
truthy( Soldier:isa(man) )

local soldier = Soldier.new{ name = 'Dude' }
truthy( soldier:isa 'Soldier', "Soldier" )
truthy( soldier:isa 'Person' )
truthy( soldier:isa(Person) )
truthy( soldier:isa(man) )
truthy( mc.has( Soldier, 'name' ) )
truthy( mc.has( Soldier, 'force' ) )
equals( soldier.force, 3 )
truthy( soldier:walk() )
truthy( soldier:attack() )

local general = General.new{ name = 'Smith' }
truthy( general:isa 'General', "General" )
truthy( general:isa 'Soldier' )
truthy( general:isa 'Person' )
truthy( mc.has( General, 'name' ) )
truthy( mc.has( General, 'force' ) )
equals( general.force, 5 )
truthy( general:walk() )
truthy( general:attack() )

error_matches([[man = Person.new{ name = 'John' }; man:isa {}]],
        "bad argument #2 to isa %(string or Object/Class expected%)")

