#!/usr/bin/env lua

require 'Coat'

class 'Foo'

has.x = { is = 'rw', required = true }
has.y = { is = 'rw', isa = 'number' }
has.z = { is = 'ro', default = 42 }

class 'Bar'
extends 'Foo'

has.x = { '+', default = 42 }
has.y = { '+', default = 42 }
has.z = { '+', isa = 'number' }

class 'Baz'
extends 'Foo'

has.x = { '+', required = false }
has.y = { '+', isa = 'string' }
has.z = { '+', is = 'rw' }


require 'Test.Assertion'

plan(12)

if os.getenv "GEN_PNG" and os.execute "dot -V" == 0 then
    local f = io.popen("dot -T png -o 106.png", 'w')
    f:write(require 'Coat.UML'.to_dot())
    f:close()
end

local foo = Foo{ x = 0, y = 0 }
truthy( foo:isa 'Foo', "Foo" )
equals( foo.x, 0 )
equals( foo.y, 0 )
equals( foo.z, 42 )

local bar = Bar{}
truthy( bar:isa 'Bar', "Bar" )
truthy( bar:isa 'Foo' )
equals( bar.x, 42 )
equals( bar.y, 42 )
equals( bar.z, 42 )

error_matches([[baz = Baz()]],
        "Attribute 'x' is required",
        "required")

error_matches([[baz = Baz{ x = 0, y = 'text' }]],
        "Invalid type for attribute 'y' %(got string, expected number%)",
        "isa")

error_matches([[baz = Baz{ x = 0 }; baz.z = 0]],
        "Cannot set a read%-only attribute %(z%)",
        "ro")

