#!/usr/bin/env lua

require 'Coat'

class 'Engine'

function method._break ()
    _G.seen = "I broke"
end

class 'Car'
has.engine = { is = 'rw', isa = 'Engine', handles = { '_break' } }

require 'Test.Assertion'

plan(5)

if os.getenv "GEN_PNG" and os.execute "dot -V" == 0 then
    local f = io.popen("dot -T png -o 131.png", 'w')
    f:write(require 'Coat.UML'.to_dot())
    f:close()
end

_G.seen = ''

local car = Car.new{ engine = Engine.new() }
truthy( car:isa 'Car', "isa Car" )
truthy( car.engine:isa 'Engine' )
truthy( car:can '_break', "can _break" )
car:_break()
equals( _G.seen, "I broke" )

function Car.method:run ()
    return true
end
error_matches([[Car.has.turbo = { is = 'rw', handles = { 'run' } }]],
        "Duplicate definition of method run")
