#!/usr/bin/env lua

require 'Coat.Role'

class 'MyApp.Foo.Bar'
has.baz = { is = 'rw' }

require 'Test.Assertion'

plan(2)

if os.getenv "GEN_PNG" and os.execute "dot -V" == 0 then
    local f = io.popen("dot -T png -o 113.png", 'w')
    f:write(require 'Coat.UML'.to_dot())
    f:close()
end

local foo = MyApp.Foo.Bar.new()
truthy( foo:isa 'MyApp.Foo.Bar' )
foo.baz = 'baz'
equals( foo.baz, 'baz' )

