#!/usr/bin/env lua

require 'Coat'

augment 'Point'

has.color = { is = 'rw', isa = 'number' }

require 'Test.Assertion'

plan(6)

if os.getenv "GEN_PNG" and os.execute "dot -V" == 0 then
    local f = io.popen("dot -T png -o 123.png", 'w')
    f:write(require 'Coat.UML'.to_dot())
    f:close()
end

local p = Point{x = 1, y = 2, color = 0xFF00FF}
equals( p:type(), 'Point', "new" )
truthy( p:isa 'Point' )
equals( p.x, 1 )
equals( p.y, 2 )
equals( p.color, 0xFF00FF )
equals( tostring(p), "(1, 2)", "stringify" )

