#!/usr/bin/env lua

require 'Coat'

class 'Car'

has.engine = { is = 'ro', isa = 'string', lazy = true, builder = '_build_engine' }

function method._build_engine ()
    return "Engine"
end

class 'SpecialCar'
extends 'Car'

function override._build_engine ()
    return "SpecialEngine"
end

require 'Test.Assertion'

plan(8)

if os.getenv "GEN_PNG" and os.execute "dot -V" == 0 then
    local f = io.popen("dot -T png -o 103.png", 'w')
    f:write(require 'Coat.UML'.to_dot())
    f:close()
end

local car = Car.new()
truthy( car:isa 'Car', "Car" )
equals( car.engine, 'Engine' )

car = SpecialCar.new()
truthy( car:isa 'SpecialCar', "SpecialCar" )
truthy( car:isa 'Car' )
equals( car.engine, 'SpecialEngine' )

SpecialCar.has.turbo = { is = 'ro', builder = '_build_turbo' }
error_matches([[local car = SpecialCar.new(); local turbo = car.turbo]],
        "method _build_turbo not found in SpecialCar")

error_matches([[Car.has.turbo = { is = 'ro', builder = function () return true end }]],
        "The builder option requires a string %(method name%)")

error_matches([[Car.has.turbo = { is = 'ro', builder = '_build_turbo', default = true }]],
        "The options default and builder are not compatible")

