-- file RealBillingServiceTest.lua
require 'Coat'

class 'RealBillingServiceTest'
extends 'BillingService'
bind.CreditCardProcessor = 'FakeCreditCardProcessor'
bind.TransactionLog = 'InMemoryTransactionLog'
