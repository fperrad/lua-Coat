#!/usr/bin/env lua

require 'Coat'

class 'Foo'

has.read = { is = 'ro', isa = 'number' }
has.write = { is = 'rw', default = 3, isa = 'number' }
has.reset = { is = 'ro', reset = true }

require 'Test.Assertion'

plan(22)

if os.getenv "GEN_PNG" and os.execute "dot -V" == 0 then
    local f = io.popen("dot -T png -o 014.png", 'w')
    f:write(require 'Coat.UML'.to_dot())
    f:close()
end

local foo = Foo.new{ read = 4, write = 5, reset = 7 }
equals( foo.read, 4, "Foo" )
equals( foo.write, 5 )
foo.write = 7
equals( foo.write, 7 )
equals( foo.reset, 7 )
foo.reset = nil
equals( foo.reset, nil)
foo.reset = 12
equals( foo.reset, 12 )

error_matches([[local foo = Foo.new{ read = 4, write = 5, reset = 7 }; foo.read = 5]],
        "Cannot set a read%-only attribute %(read%)")

error_matches([[local foo = Foo.new{ read = 4, write = 5, reset = 7 }; foo.read = nil]],
        "Cannot set a read%-only attribute %(read%)")

error_matches([[local foo = Foo.new{ read = 4, write = 5, reset = 7 }; foo.reset = 12]],
        "Cannot set a read%-only attribute %(reset%)")

foo = Foo.new()
equals( foo.read, nil, "Foo (default)" )
foo.read = 2
equals( foo.read, 2 )
equals( foo.write, 3 )
foo.write = 7
equals( foo.write, 7 )
foo.reset = 7
equals( foo.reset, 7 )
foo.reset = nil
equals( foo.reset, nil )
foo.reset = 12
equals( foo.reset, 12 )

error_matches([[local foo = Foo.new(); foo.read = 2; foo.read = 5]],
        "Cannot set a read%-only attribute %(read%)")

error_matches([[local foo = Foo.new(); foo.read = 2; foo.read = nil]],
        "Cannot set a read%-only attribute %(read%)")

error_matches([[local foo = Foo.new(); foo.reset = 7; foo.reset = 12]],
        "Cannot set a read%-only attribute %(reset%)")

error_matches([[local foo = Foo.new(); foo.bad = 5]],
        "Cannot set 'bad' %(unknown%)")

equals( foo.bad, nil )

error_matches([[Foo.has.field = { is = 'ro', reset = true, required = true }]],
        "The reset option is incompatible with required option")

