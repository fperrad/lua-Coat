#!/usr/bin/env lua

require 'Coat'

singleton 'Foo'

has.var = { is = 'rw', isa = 'number' }

require 'Test.Assertion'

plan(6)

if os.getenv "GEN_PNG" and os.execute "dot -V" == 0 then
    local f = io.popen("dot -T png -o 121.png", 'w')
    f:write(require 'Coat.UML'.to_dot())
    f:close()
end

local foo = Foo.instance{ var = 4 }
truthy( foo:isa 'Foo', "Foo" )
equals( foo.var, 4 )

local bar = Foo.instance()
truthy( bar:isa 'Foo' )
equals( foo, bar )

local baz = Foo()
truthy( baz:isa 'Foo' )
equals( foo, baz )

