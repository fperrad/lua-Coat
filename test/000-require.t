#!/usr/bin/env lua

require 'Test.Assertion'

plan(9)

if not require_ok 'Coat' then
    BAIL_OUT "no lib"
end

local m = require 'Coat'
is_table( m )
equals( m, package.loaded.Coat )
matches( m._COPYRIGHT, 'Perrad', "_COPYRIGHT" )
matches( m._DESCRIPTION, 'Lua', "_DESCRIPTION" )
is_string( m._VERSION, "_VERSION" )
matches( m._VERSION, '^%d%.%d%.%d$' )

equals( m.math, nil, "check ns pollution" )

local function err ()
    m.error "MSG" -- line 22
end
local _, msg = pcall(err)
matches( msg, "000%-require%.t:22: MSG", "error" )

