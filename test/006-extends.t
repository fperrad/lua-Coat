#!/usr/bin/env lua

require 'Coat'

class 'MyItem'

has.name = { is = 'rw', isa = 'string' }

class 'MyItem3D'

extends( 'Point3D', MyItem )

require 'Test.Assertion'

plan(19)

if os.getenv "GEN_PNG" and os.execute "dot -V" == 0 then
    local f = io.popen("dot -T png -o 006.png", 'w')
    f:write(require 'Coat.UML'.to_dot())
    f:close()
end

local mc = require 'Coat.Meta.Class'

local point2d = Point.new{ x = 2, y = 4}
equals( point2d:type(), 'Point', "Point" )
truthy( point2d:isa 'Point' )
falsy( mc.has( Point, 'z' ) )

local point3d = Point3D.new{ x = 1, y = 3, z = 1}
equals( point3d:type(), 'Point3D', "Point3D" )
truthy( point3d:isa 'Point3D' )
truthy( mc.has( Point3D, 'z' ) )

local item = MyItem3D.new{ name = 'foo', x = 4, z = 3 }
equals( item:type(), 'MyItem3D', "MyItem3D" )
truthy( item:isa 'MyItem3D' )
truthy( item:isa 'Point3D' )
truthy( item:isa 'MyItem' )
equals( item.x, 4 )
equals( item.y, 0 )
equals( item.z, 3 )
equals( item.name, 'foo' )
equals( item:dump(), [[
obj = MyItem3D {
  name = "foo",
  x = 4,
  y = 0,
  z = 3,
}]], "dump" )

local t = {}
for k in pairs(item) do
    t[#t+1] = k
end
table.sort(t)
if _VERSION == 'Lua 5.1' then
    todo("__pairs only with Lua 5.2", 1)
end
equals( table.concat(t, ','), 'name,x,y,z', "__pairs" )

error_matches([[MyItem3D.extends {}]],
        "bad argument #1 to extends %(string or Class expected%)")

error_matches([[MyItem.extends 'MyItem3D']],
        "Circular class structure between 'MyItem' and 'MyItem3D'")

error_matches([[class 'MyItem3D']],
        "name conflict for module 'MyItem3D'")

