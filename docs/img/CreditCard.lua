-- file CreditCard.lua
require 'Coat'

class 'CreditCard'
has.BIN             = { is = 'ro', isa = 'string', required = true }
has.monthValidity   = { is = 'ro', isa = 'number', required = true }
has.yearValidity    = { is = 'ro', isa = 'number', required = true }

class 'ChargeResult'
has.wasSuccessful   = { is = 'ro', isa = 'boolean', required = true }
has.declineMessage  = { is = 'ro', isa = 'string' }
