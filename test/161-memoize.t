#!/usr/bin/env lua

require 'Coat'

class 'Foo'
has.cntCall = { is = 'rw', default = 0 }

function overload:__tostring ()
    return self._CLASS
end

function method:sum (...)
    self.cntCall = self.cntCall + 1
    local arg = {...}
    local res = 0
    for i = 1, #arg do res = res + arg[i] end
    return res
end


require 'Test.Assertion'

plan(17)

if os.getenv "GEN_PNG" and os.execute "dot -V" == 0 then
    local f = io.popen("dot -T png -o 161.png", 'w')
    f:write(require 'Coat.UML'.to_dot())
    f:close()
end

local foo = Foo()
truthy( foo:isa 'Foo', "Foo" )
equals( foo.cntCall, 0 )
equals( foo:sum(1, 2), 3 )
equals( foo.cntCall, 1 )
equals( foo:sum(1, 2), 3 )
equals( foo.cntCall, 2 )
Foo.memoize "sum"
equals( foo:sum(1, 2), 3 )
equals( foo.cntCall, 3 )
equals( foo:sum(1, 2), 3 )
equals( foo.cntCall, 3 )
equals( foo:sum(1, 2, 3), 6 )
equals( foo.cntCall, 4 )
equals( foo:sum(1, 2, 3), 6 )
equals( foo.cntCall, 4 )

local n = 0
for k, v in pairs(require 'Coat.Meta.Class'._CACHE) do
    diag(k .. " -> " .. tostring(v))
    n = n + 1
end
equals( n, 2 )

error_matches([[Foo.memoize {}]],
        "bad argument #1 to memoize %(string expected, got table%)")

error_matches([[Foo.memoize 'add']],
        "Cannot memoize non%-existent method add in class Foo")


