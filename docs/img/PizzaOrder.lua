-- file PizzaOrder.lua
require 'Coat'

class 'PizzaOrder'
has.amount          = { is = 'ro', isa = 'number', required = true }

require 'Coat.Types'
enum.ReceiptType    = { 'SuccessfulCharge', 'DeclinedCharge' }

class 'Receipt'
has._type           = { is = 'ro', isa = 'ReceiptType', required = true }
has.amount          = { is = 'ro', isa = 'number' }
has.message         = { is = 'ro', isa = 'string' }
