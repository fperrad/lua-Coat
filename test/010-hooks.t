#!/usr/bin/env lua

require 'Coat'

class 'Parent'

function method:pushelem (elem)
    table.insert( _G.list, elem )
end

class 'Child'
extends 'Parent'

function before:pushelem (elem)
    table.insert( _G.list, 'before1:' .. elem )
end

function before:pushelem (elem)
    table.insert( _G.list, 'before2:' .. elem )
end

function after:pushelem (elem)
    table.insert( _G.list, 'after1:' .. elem )
end

function before:pushelem (elem)
    table.insert( _G.list, 'before3:' .. elem )
end

function after:pushelem (elem)
    table.insert( _G.list, 'after2:' .. elem )
end

function after:pushelem (elem)
    table.insert( _G.list, 'after3:' .. elem )
end

require 'Test.Assertion'

plan(9)

if os.getenv "GEN_PNG" and os.execute "dot -V" == 0 then
    local f = io.popen("dot -T png -o 010.png", 'w')
    f:write(require 'Coat.UML'.to_dot())
    f:close()
end

local p = Parent.new()
truthy( p:isa 'Parent', "Simple" )
truthy( p.pushelem )
_G.list = {}
p:pushelem 'Coat'
array_equals( _G.list, { 'Coat' } )

local c = Child.new()
truthy( c:isa 'Child', "Multiple" )
truthy( c:isa 'Parent' )
truthy( c.pushelem )
_G.list = {}
c:pushelem 'Coat'
array_equals( _G.list, {
        'before3:Coat',
        'before2:Coat',
        'before1:Coat',
                'Coat',
        'after1:Coat',
        'after2:Coat',
        'after3:Coat',
} )

error_matches([[function Child.after:push_elem () end]],
        "Cannot after non%-existent method push_elem in class Child")

error_matches([[function Child.before:push_elem () end]],
        "Cannot before non%-existent method push_elem in class Child")

