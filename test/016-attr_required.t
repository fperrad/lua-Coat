#!/usr/bin/env lua

require 'Coat'

class 'Foo'
has.bar = { is = 'ro', required = true }
has.baz = { is = 'rw', default = 100, required = true }
has.boo = { is = 'rw', lazy = true, default = 50, required = true }
has.subobject = { isa = 'Bar' }

require 'Test.Assertion'

plan(13)

if os.getenv "GEN_PNG" and os.execute "dot -V" == 0 then
    local f = io.popen("dot -T png -o 016.png", 'w')
    f:write(require 'Coat.UML'.to_dot())
    f:close()
end

local foo = Foo.new{ bar = 10, baz = 20, boo = 100 }
truthy( foo:isa 'Foo', "Foo1" )
equals( foo.bar, 10 )
equals( foo.baz, 20 )
equals( foo.boo, 100 )

foo = Foo.new{ bar = 10, boo = 5 }
truthy( foo:isa 'Foo', "Foo2" )
equals( foo.bar, 10 )
equals( foo.baz, 100 )
equals( foo.boo, 5 )

foo = Foo.new{ bar = 10 }
truthy( foo:isa 'Foo', "Foo3" )
equals( foo.bar, 10 )
equals( foo.baz, 100 )
equals( foo.boo, 50 )

error_matches([[foo = Foo.new()]],
        "Attribute 'bar' is required",
        "Foo4")

