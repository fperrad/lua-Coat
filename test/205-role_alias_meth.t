#!/usr/bin/env lua

require 'Coat.Role'

role 'Foo'

local function m1 (self)
    return "m1"
end

method.m1 = m1

function method:m2 ()
    return m1(self) .. "m2"
end

class 'A'
with 'Foo'

class 'B'
with( 'Foo', { alias = { m1 = 'foo_m1', m2 = 'foo_m2' } } )

class 'C'
with( 'Foo', { alias = { m1 = 'foo_m1', m2 = 'foo_m2' },
               excludes = { 'm1', 'm2' } } )


require 'Test.Assertion'

plan(19)

if os.getenv "GEN_PNG" and os.execute "dot -V" == 0 then
    local f = io.popen("dot -T png -o 205.png", 'w')
    f:write(require 'Coat.UML'.to_dot())
    f:close()
end

local a = A.new()
truthy( a:isa 'A', "A" )
truthy( a.m1 )
truthy( a.m2 )
equals( a:m1(), 'm1' )
equals( a:m2(), 'm1m2' )

local b = B.new()
truthy( b:isa 'B', "B" )
truthy( b.m1 )
truthy( b.m2 )
truthy( b.foo_m1 )
truthy( b.foo_m2 )
equals( b:m1(), 'm1' )
equals( b:m2(), 'm1m2' )

local c = C.new()
truthy( c:isa 'C', "C" )
equals( c.m1, nil )
equals( c.m2, nil )
truthy( c.foo_m1 )
truthy( c.foo_m2 )
equals( c:foo_m1(), 'm1' )
equals( c:foo_m2(), 'm1m2' )

