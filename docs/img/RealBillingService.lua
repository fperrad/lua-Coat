-- file RealBillingService.lua
require 'Coat'

require 'PaypalCreditCardProcessor'
require 'DatabaseTransactionLog'
singleton 'RealBillingService'
extends 'BillingService'
bind.CreditCardProcessor = PaypalCreditCardProcessor
bind.TransactionLog = function ()
    return DatabaseTransactionLog.new{
        jdbcUrl = "jdbc:mysql://localhost/pizza",
        threadPoolSize = 30,
    }
end
