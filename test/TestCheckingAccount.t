#!/usr/bin/env lua

require 'CheckingAccount'
require 'Test.Assertion'

plan(20)

if not require_ok 'CheckingAccount' then
    skip_rest "no lib"
    os.exit()
end

if os.getenv "GEN_PNG" and os.execute "dot -V" == 0 then
    local f = io.popen("dot -T png -o CheckingAccount.png", 'w')
    f:write(require 'Coat.UML'.to_dot())
    f:close()
end

local savings_account = BankAccount{ balance = 250 }
truthy( savings_account:isa 'BankAccount', "BankAccount" )
equals( savings_account.balance, 250 )
savings_account:withdraw( 50 )
equals( savings_account.balance, 200 )
savings_account:deposit( 150 )
equals( savings_account.balance, 350 )

savings_account = BankAccount{ balance = 350 }
local checking_account = CheckingAccount{ balance = 100, overdraft_account = savings_account }
truthy( checking_account:isa 'CheckingAccount', "CheckingAccount" )
truthy( checking_account:isa 'BankAccount' )
equals( checking_account.overdraft_account, savings_account )
equals( checking_account.balance, 100 )
checking_account:withdraw(50)
equals( checking_account.balance, 50 )
equals( savings_account.balance, 350 )
checking_account:withdraw(200)
equals( checking_account.balance, 0 )
equals( savings_account.balance, 200 )

checking_account = CheckingAccount{ balance = 100 }
truthy( checking_account:isa 'CheckingAccount', "CheckingAccountSimple" )
truthy( checking_account:isa 'BankAccount' )
equals( checking_account.overdraft_account, nil )
equals( checking_account.balance, 100 )
equals( checking_account.balance, 100 )
checking_account:withdraw(50)
equals( checking_account.balance, 50 )
error_matches(function () checking_account:withdraw(200) end,
        "Account overdrawn")

