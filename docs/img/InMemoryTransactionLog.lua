-- file InMemoryTransactionLog
require 'Coat'
class 'InMemoryTransactionLog'
with 'TransactionLog'

has.log             = { is = 'rw', isa = 'table<ChargeResult>',
                        default = function () return {} end }
has.lastRecord      = { is = 'rw', isa = 'ChargeResult' }

function method:logChargeResult (result)
    self.lastRecord = result
    table.insert(self.log, result)
end
