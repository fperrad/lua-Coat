-- file BillingService.lua
require 'Coat'

require 'Receipt'
abstract 'BillingService'

has.processor       = { is = 'ro', does = 'CreditCardProcessor', inject = true }
has.transactionLog  = { is = 'ro', does = 'TransactionLog', inject = true }

function method:chargeOrder (order, creditCard)
    local result = self.processor:charge(creditCard, order.amount)
    self.transactionLog:logChargeResult(result)
    if result.wasSuccessful then
        return Receipt.new{
            _type = 'SuccessfulCharge',
            amount = order.amount,
        }
    else
        return Receipt.new{
            _type = 'DeclinedCharge',
            message = result.declineMessage,
        }
    end
end
