#!/usr/bin/env lua

require 'Coat'

class 'Parent'
has.foo = { is = 'rw' }
function method:bar () return 'bar' end
function method:baz () return 'baz' end

class 'Child'
extends 'Parent'
function override:bar () return 'BAR' end

require 'Test.Assertion'

plan(15)

if os.getenv "GEN_PNG" and os.execute "dot -V" == 0 then
    local f = io.popen("dot -T png -o 102.png", 'w')
    f:write(require 'Coat.UML'.to_dot())
    f:close()
end

local p = Parent.new()
truthy( p:isa 'Parent', "Parent" )
truthy( p.bar )
truthy( p.baz )
equals( p:bar(), 'bar' )
equals( p:baz(), 'baz' )

local c = Child.new()
truthy( c:isa 'Child', "Child" )
truthy( c:isa 'Parent' )
truthy( c.bar )
truthy( c.baz )
equals( c:bar(), 'BAR' )
equals( c:baz(), 'baz' )

error_matches([[Child.override.biz = function (self) return 'BIZ' end]],
        "Cannot override non%-existent method biz in class Child",
        "bad 1")

error_matches([[Child.method.baz = function (self) return 'baz' end]],
        "Duplicate definition of method baz",
        "bad 2")

error_matches([[Parent.method.baz = function (self) return 'baz' end]],
        "Duplicate definition of method baz",
        "bad 3")

error_matches([[Child.method.foo = function () end]],
        "Overwrite definition of attribute foo",
        "bad 4")

