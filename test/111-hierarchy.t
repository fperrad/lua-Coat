#!/usr/bin/env lua

require 'Test.Assertion'

plan(7)

if not require_ok 'MyApp' then
    skip_rest "no lib"
    os.exit()
end

if os.getenv "GEN_PNG" and os.execute "dot -V" == 0 then
    local f = io.popen("dot -T png -o 111.png", 'w')
    f:write(require 'Coat.UML'.to_dot())
    f:close()
end

local bar = MyApp.Bar.new()
truthy( bar:isa 'MyApp.Bar', "MyApp.Bar" )

local foo = MyApp.Bar.Foo.new()
truthy( foo:isa 'MyApp.Bar.Foo', "MyApp.Bar.Foo" )
foo.bar = 'bar'
equals( foo.bar, 'bar' )

local baz = MyApp.Baz.new()
truthy( baz:isa 'MyApp.Baz', "MyApp.Baz" )

foo = MyApp.Baz.Foo.new()
truthy( foo:isa 'MyApp.Baz.Foo', "MyApp.Baz.Foo" )
foo.baz = 'baz'
equals( foo.baz, 'baz' )

