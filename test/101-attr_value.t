#!/usr/bin/env lua

require 'Coat'

class 'Foo'

has.attr = { is = 'rw' }

require 'Test.Assertion'

plan(5)

if os.getenv "GEN_PNG" and os.execute "dot -V" == 0 then
    local f = io.popen("dot -T png -o 101.png", 'w')
    f:write(require 'Coat.UML'.to_dot())
    f:close()
end

local foo = Foo.new()
equals( foo.attr, nil )
foo.attr = 2
equals( foo.attr, 2 )
foo.attr = false
equals( foo.attr, false )
foo.attr = nil
equals( foo.attr, nil )

foo = Foo.new{ attr = function () return 42 end }
equals( foo.attr, 42 )
