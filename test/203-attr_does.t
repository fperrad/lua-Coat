#!/usr/bin/env lua

require 'Coat.Role'

role 'Breakable'

has.is_broken = { is = 'rw', isa = 'boolean' }

function method:_break ()
    self.is_broken = true
end

class 'Engine'
with 'Breakable'

class 'SpecialEngine'
extends 'Engine'

class 'Car'
has.engine = { is = 'rw', does = 'Breakable' }

require 'Test.Assertion'

plan(6)

if os.getenv "GEN_PNG" and os.execute "dot -V" == 0 then
    local f = io.popen("dot -T png -o 203.png", 'w')
    f:write(require 'Coat.UML'.to_dot())
    f:close()
end

local car = Car.new{ engine = Engine.new() }
truthy( car:isa 'Car', "Car" )
truthy( car.engine:does 'Breakable' )
equals( car.engine.is_broken, nil )
car.engine:_break()
truthy( car.engine.is_broken )
error_matches([[local car = Car.new{ engine = Engine.new() }; car.engine = Car.new()]],
        "Value for attribute 'engine' does not consume role 'Breakable'")
car.engine = SpecialEngine.new()
truthy( car.engine:does 'Breakable' )

