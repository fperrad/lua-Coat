-- file test.lua
require 'RealBillingServiceTest'
require 'PizzaOrder'
require 'CreditCard'
local billingService = RealBillingServiceTest()
assert(billingService.processor:isa 'FakeCreditCardProcessor')
local order = PizzaOrder.new{
    amount = 100,
}
local creditCard = CreditCard.new{
    BIN = "12345678901234567",
    monthValidity = 11,
    yearValidity = 2010,
}
billingService.processor.nextResult = true
local receipt = billingService:chargeOrder(order, creditCard)
assert(receipt._type == 'SuccessfulCharge')
assert(receipt.amount == 100)
assert(billingService.processor.lastAmount == 100)
assert(billingService.processor.lastCard.yearValidity == 2010)
local record = billingService.transactionLog.lastRecord
assert(record.wasSuccessful == true)
